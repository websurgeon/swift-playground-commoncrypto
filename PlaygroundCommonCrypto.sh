#!/bin/sh
# This script creates a fake CommonCrypto.framework directory that is made available to the Swift Playground environment
# The fake framework contains a module.map file pointing it to The actual CommonCrypto framework in Xcode simulator sdk
# 
# To 'undo' the process of this script, simply remove the CommonCrypto.framework directory from the destination directory ($FRAMEWORKS_DIR)
# 	e.g.	$ sudo rm -rf /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator.sdk/System/Library/Frameworks
# This methodology has been taken from http://iosdeveloperzone.com/2014/10/03/using-commoncrypto-in-swift/

CURRENT_DIR=$(pwd)
SCRIPT_DIR=$(dirname $0)
FRAMEWORKS_DIR=`xcrun -sdk iphonesimulator -show-sdk-path`/System/Library/Frameworks
FRAMEWORK_NAME="CommonCrypto.framework"

if [ -d "$FRAMEWORKS_DIR/$FRAMEWORK_NAME" ]; then
	echo "\n$FRAMEWORK_NAME alread exists! \n\nSee $FRAMEWORKS_DIR/$FRAMEWORK_NAME"
	echo "\nNO ACTION TAKEN"
else
	echo "sudo permission is required to copy the framework to your simulators frameworks directory"

	sudo cp -r "$SCRIPT_DIR/$FRAMEWORK_NAME" "$FRAMEWORKS_DIR/$FRAMEWORK_NAME"
	echo "$FRAMEWORK_NAME Created!"
	echo "See $FRAMEWORKS_DIR/$FRAMEWORK_NAME"
	echo "OK"
fi
