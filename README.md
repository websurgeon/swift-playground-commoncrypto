# Swift Playground with CommonCrypto

A script to make CommonCrypto framework available in Swift Playground 

### Usage

Command line:
	$ sh PlaygroundCommonCrypto.sh


In playground:

	import CommonCrypto